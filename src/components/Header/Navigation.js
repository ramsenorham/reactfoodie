// src/components/Header/Navigation.js
import React from 'react';
import {Link } from 'react-router-dom';
import '../../assets/styles/Navigation.css';

function Navigation() {
  return (
    <nav className="navbar navbar-expand-lg navbar-light">{/*navbar-dark bg-dark bg-light*/}
      <button className="navbar-toggler d-lg-none" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span className="navbar-toggler-icon"></span>
      </button>
      <div className="collapse navbar-collapse" id="navbarNav">
        <ul className="navbar-nav ml-auto">
          <li className="nav-item active">
            <Link className="nav-link" to="/">Home</Link>
          </li>
          <li className="nav-item">
            <Link className="nav-link" to="/lunch">Mittagstisch</Link>
          </li>
          <li className="nav-item">
            <Link className="nav-link" to="/menu">Speisekarte</Link>
          </li>
          <li className="nav-item">
            <Link className="nav-link" to="/contact">Kontakt</Link>
          </li>
        </ul>
      </div>
    </nav>
  );
}

export default Navigation;
/*
<button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation" >
*/