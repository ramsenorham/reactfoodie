// src/components/Header/Header.js
import React from 'react';
import { Link } from 'react-router-dom';
import Navigation from './Navigation';
import '../../assets/styles/Header.css';

function Header() {
  return (
    <header>
      <div className="container">
        <div className="row">
          <div className="col-6">
            <Link className="navbar-brand" to="/">Hier Logo</Link>
          </div>
          <div className="col-6 text-center">
            <h1>Gastronomie</h1>
          </div>
        </div>
      </div>
      <Navigation />
    </header>
  );
}

export default Header;
