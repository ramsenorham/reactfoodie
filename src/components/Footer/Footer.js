// src/components/Footer/Footer.js
import React from 'react';
import '../../assets/styles/Footer.css';

function Footer() {
  return (
    <footer className="bg-dark text-light p-4">
      <div className="container">
      <div className="row">
        <div className="col-md-6">
          <p className="mb-0">Impressum // Datenschutzerklärung</p>
        </div>
        <div className="col-md-6 text-md-right">
          <div className="footer-links facebook-icon">
            <a href="https://www.facebook.com/ramsen.orham" className="text-light">Facebook <em className="fab fa-facebook fa-lg"></em></a>
          </div>
        </div>
      </div>
      </div>
    </footer>
  );
}

export default Footer;
