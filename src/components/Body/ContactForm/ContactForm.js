import React from "react";
import '../../../assets/styles/ContactForm.css';
import PersonalData from "./PersonalData";
import RequestSection from "./RequestSection";
import CaptchaAndAgreement from "./CaptchaAndAgreement";

class ContactForm extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      name : '',
      contactData: '',
      antwort: 'perEmail',
      chooseSubject: '',
      message: '',
      captcha: '',
      agb: false,
    };
  }

  // Funktion, die aufgerufen wird, wenn sich der Wert eines Formularelements ändert.
  handleInputChange = (event) => {
    
    const { name, value, type, checked } = event.target;
    //console.log({ name, value, type, checked });
    if (type === 'checkbox') {
      this.setState({ [name]: checked });
    } else {
      //es handelt sich hier um ein Textfeld, Textbereich, Auswahlmenü usw.
      this.setState({ [name]: value });
    }
  };

  handleSubmit = async (event) => {
    event.preventDefault();

    const { name, contactData, antwort, chooseSubject, message, captcha, agb, } = this.state;

    const formData = new FormData();
    formData.append('name', name);
    formData.append('contactData', contactData);
    formData.append('antwort', antwort);
    formData.append('chooseSubject', chooseSubject);
    formData.append('message', message);
    formData.append('captcha', captcha);
    formData.append('agb', agb);

    console.log(formData);

    if (this.state.captcha === "1234") {
      const response = await fetch('/send-email', {
        method: 'POST',
        body: formData,
      });

      if (response.ok) {
        const successMessage = await response.text();
        alert('Erfolgreiche: ' + successMessage);
      } else {
        const errorMessage = await response.text();
        alert('Fehler: ' + errorMessage);
      }
    } else {
      alert('Captcha Fehlerhaft!');
    }
  };

  render() {
    return (
      <div id="contact" className="person-info container mt-4">
        <form name="contForm" onSubmit={this.handleSubmit}>
          <h2 className="mb-4">Kontakt</h2>
          <h4>Bitte geben Sie Ihre Daten ein:</h4>
  
          {/* Persönliche Daten */}
          <PersonalData handleInputChange={this.handleInputChange} antwort={this.state.antwort} name={this.state.name} contactData={this.state.contactData} />

          {/* Anfrage */}
          <RequestSection handleInputChange={this.handleInputChange} chooseSubject={this.state.chooseSubject} message={this.state.message} />

          {/* Captcha */}
          <CaptchaAndAgreement captcha={this.state.captcha} handleInputChange={this.handleInputChange} agb={this.state.agb} />
          
          {/* Submit-Button */}
          <p>
            {/*<input type="submit" value="Senden" />*/}
            <button type="submit" className="btn btn-primary">Senden</button>
          </p>
        </form>
      </div>
    );
  }
}

export default ContactForm;
