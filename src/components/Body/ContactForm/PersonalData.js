// Komponente für die persönlichen Daten
import React from 'react';

const PersonalData = ({ handleInputChange, antwort, name, contactData }) => {
  return (
  <fieldset>
    <legend>Persönliche Daten</legend>
    <p>
      <label htmlFor="name">Name:</label>
      <input type="text" name="name" id="name" className="form-control" placeholder="Name" required value={name} onChange={handleInputChange} />
    </p>
    <div>
      Antwort bitte per &nbsp;
      <div className="form-check form-check-inline">
        <label className="form-check-label" htmlFor="perEmail">E-Mail</label>
        <input type="radio" name="antwort" id="perEmail" className="form-check-input" value="perEmail" checked={antwort === 'perEmail'} onChange={handleInputChange} />
      </div>
      <div className="form-check form-check-inline">
          <label className="form-check-label" htmlFor="perTelefon">Telefon</label>
          <input type="radio" name="antwort" id="perTelefon" className="form-check-input" value="perTelefon" checked={antwort === 'perTelefon'} onChange={handleInputChange} />
      </div>
    </div>
    <p>
      <label htmlFor="contactData">Kontaktdaten:</label>
      <input type={antwort === 'perEmail' ? 'email' : 'tel'} name="contactData" id="contactData" className="form-control" placeholder={antwort === 'perEmail' ? 'E-Mail' : 'Telefon'} required value={contactData} onChange={handleInputChange} />
    </p>
  </fieldset>
 );
};

export default PersonalData;