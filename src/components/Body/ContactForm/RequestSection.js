// Komponente für die Anfrage
import React from 'react';

const topicOptions = [
  {
    optgroup: "Anfragen im Gastronomie-Bereich:",
    options: [
      "Tischreservierung",
      "Anfrage zu Catering-Dienstleistungen",
      "Feedback zu Ihrem Restaurantbesuch",
      "Veranstaltungsanfrage",
      "Allgemeine Anfrage zum Menü",
    ],
  },
  {
    optgroup: "Sonstige Anfragen:",
    options: [
      "Allgemeine Anfrage",
      "Feedback zu unserem Service",
    ],
  },
];

const RequestSection = ({ handleInputChange, chooseSubject, message }) => {
  return (
    <fieldset>
      <legend>Anfrage</legend>
      <p>
        <label htmlFor="chooseSubject">Thema:</label>
        <select name="chooseSubject" id="chooseSubject" className="form-control" value={chooseSubject} onChange={handleInputChange} >
          <option value="">- Bitte wählen -</option>
          {topicOptions.map((group, index) => (
            <optgroup key={index} label={group.optgroup}>
              {group.options.map((optionText, optionIndex) => (
                <option key={optionIndex} value={optionText}>
                  {optionText}
                </option>
              ))}
            </optgroup>
          ))}
        </select>
      </p>
      <p>
        <label htmlFor="message">Anfrage:</label>
        <textarea name="message" id="message" className="form-control" rows="5" placeholder="Anfrage" required value={message} onChange={handleInputChange}></textarea>
      </p>
    </fieldset>
  );
}

export default RequestSection;
