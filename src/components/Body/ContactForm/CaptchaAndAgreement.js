// Komponente für das Captcha und AGB
import React from 'react';

const CaptchaAndAgreement = ({ captcha, handleInputChange, agb }) => {
  return (
  <>
    <p>Captcher-Code: {"1234"}</p>
    <p>
      <label htmlFor="captcha">Zahl!:</label>
      <input type="text" name="captcha" id="captcha" className="form-control" placeholder="Zahl oben eintragen!" required value={captcha} onChange={handleInputChange} />
    </p>

    {/* AGB Checkbox */}
    <div className="form-check">
      <label className="form-check-label" htmlFor="agb">Ich erkenne die AGB an.</label>
      <input type="checkbox" className="form-check-input" name="agb" id="agb" value="anerkant" required checked={agb} onChange={handleInputChange} />
    </div>
  </>
  );
};

export default CaptchaAndAgreement;
