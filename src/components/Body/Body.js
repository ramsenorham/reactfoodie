// src/components/Body/Body.js
import React from 'react';
import { Routes, Route } from 'react-router-dom';
import Home from './Home/Home';
import Lunch from './Lunch/Lunch';
import Menu from './Menu/Menu';
import ContactForm from './ContactForm/ContactForm';

function Body() {
  return (
    <Routes>
      {/* je nach Navi wird bestimmte Seite hier in Body angezeigt: Home, Lunch, Menu oder ContactForm Seite */}
      <Route path="/" element={<Home />} />
      <Route path="/lunch" element={<Lunch />} />
      <Route path="/menu" element={<Menu />} />
      <Route path="/contact" element={<ContactForm />} />
    </Routes>
  );
}

export default Body;
