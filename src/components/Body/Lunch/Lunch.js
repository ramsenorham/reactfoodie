import React from 'react';
import '../../../assets/styles/Lunch.css';
import lunch from '../../../utils/lunch.json';

function Lunch() {

  const handleOrder = () => {
    // Implementieren Sie die Bestellfunktionalität hier
    alert('Ihre Bestellung wurde erfolgreich aufgenommen! Vielen Dank!');
  };

  return (
    <section id="lunch" className="container mt-4">
      <h2 className="mb-4">Unser Mittagstisch</h2>
      <p>Entdecke unsere wöchentlich wechselnden Gerichte.</p>
      <div className="row">
        {lunch.map((dish, index) => (
          <div key={index} className="col-md-4 mb-4">
            <div className="card">
              <img src={dish.image} className="card-img-top" alt={dish.name} />
              <div className="card-body">
                <h5 className="card-title">{dish.name}</h5>
                <p className="card-text"><strong>Preis:</strong> {dish.price} €</p>
                <p className="card-text"><strong>Beschreibung:</strong> {dish.description}</p>
                <button className="btn btn-primary" onClick={handleOrder}>Jetzt bestellen</button>
              </div>
            </div>
          </div>
        ))}
      </div>
      <p className="mt-4">Mittagstisch am Arbeitsplatz? Einfach vorbestellen und abholen. <a href="tel:0123456789">Anrufen 0123456789<em className="fa fa-phone fa-lg" aria-hidden="true"></em></a>...</p>
    </section>
  );
}

export default Lunch;
