// src/components/Home/Address_OpeningHours_Telefon.js
import React from 'react';

function Address_OpeningHours_Telefon() {
  return (
    <div className="mt-4 d-flex flex-wrap justify-content-around">
      <div className="mt-4">
        <h2><em className="fa fa-home fa-lg" aria-hidden="true"></em> Adresse</h2>
        <p>Muster Straße 99,</p>
        <p>12345 Musterstadt</p>
      </div>
      <div className="mt-4">
        <h2><em className="fa fa-info-circle fa-lg" aria-hidden="true"></em> Öffnungszeiten</h2>
        <p>Montag bis Donnerstag: 09:00 – 22:00 Uhr</p>
        <p>Freitag bis Sonntag: 09:00 – 00:00 Uhr</p>
      </div>
      <div className="mt-4">
        <h2><em className="fa fa-phone fa-lg" aria-hidden="true"></em> Telefon</h2>
        <p>0123456789</p>
      </div>
    </div>
  );
}

export default Address_OpeningHours_Telefon;
