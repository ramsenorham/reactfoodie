// src/components/Home/GastronomyDescription.js
import React from 'react';

function GastronomyDescription() {
  return (
    <div>
      <h2 className="mb-4">Gastronomie Beschreibung</h2>
      <p>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nec odio. Praesent libero. Sed cursus ante dapibus diam.
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nec odio. Praesent libero. Sed cursus ante dapibus diam.
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nec odio.
      </p>
      {/* Füge hier weitere Abschnitte der Gastronomie-Beschreibung hinzu */}
    </div>
  );
}

export default GastronomyDescription;
