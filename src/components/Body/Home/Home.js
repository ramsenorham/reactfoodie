// src/components/Home/Home.js
import React from 'react';
import GastronomyDescription from './GastronomyDescription';
import ImageSlider from './ImageSlider';
import Address_OpeningHours_Telefon from './Address_OpeningHours_Telefon';
import Map from './Map';

function Home() {
  return (
    <div className="container mt-4">
      <GastronomyDescription />
      <ImageSlider />
      <Address_OpeningHours_Telefon />
      <Map />
    </div>
  );
}

export default Home;
