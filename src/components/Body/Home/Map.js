// src/components/Home/Map.js
import React, { useState } from 'react';

function Map() {
  const [mapVisible, setMapVisible] = useState(false);
  const [googleMapsUnlocked, setGoogleMapsUnlocked] = useState(false);

  const loadMap = () => {
    setMapVisible(true);
  };

  const handleCheckboxChange = () => {
    setGoogleMapsUnlocked(!googleMapsUnlocked);
  };
  //ToDo: button für karte laden, checkbox hacken für Maps immer entsperren
  return (
    <div className="mt-4 border rounded p-3">
      <p>Mit dem Laden der Karte akzeptieren Sie die Datenschutzerklärung von Google. 
        <a href="https://policies.google.com/privacy" target="_blank" rel="noopener noreferrer"> Mehr erfahren </a>
      </p>
      <button className="btn btn-primary" onClick={loadMap}>{/*mb-3*/}
        Karte laden
      </button>
      <div className="form-check mt-3">
        <label className="form-check-label" htmlFor="googleMapsCheckbox">
          <input
            type="checkbox"
            className="form-check-input"
            id="googleMapsCheckbox"
            checked={googleMapsUnlocked}
            onChange={handleCheckboxChange}
          />
          Google Maps immer entsperren.
        </label>
      </div>
      {/* Hier kannst du deine Kartenelemente einfügen (z.B., Google Maps, OpenStreetMap, etc.) */}
      {mapVisible && googleMapsUnlocked && (
        <iframe
          title='map'
          className="gmaps"
          src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d44162.403141692244!2d6.99141572357192!3d49.23403947162507!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sde!2sde!4v1689928842516!5m2!1sde!2sde"
          width="80%"
          height="400"
          style={{ border: '0' }}
          allowFullScreen=""
          loading="lazy"
          referrerPolicy="no-referrer-when-downgrade"
        ></iframe>
      )}
    </div>
  );
}

export default Map;
