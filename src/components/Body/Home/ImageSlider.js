// src/components/Home/ImageSlider.js
import React from 'react';
import Carousel from 'react-bootstrap/Carousel';
import '../../../assets/styles/ImageSlider.css';

const sliderData = [
    {
      src: 'https://images.unsplash.com/photo-1551963831-b3b1ca40c98e',
      alt: 'Breakfast',
      title: 'Breakfast',
      description: 'Beschreibung für Bild 1.',
      author: '@bkristastucchio',
    },
    {
      src: 'https://images.unsplash.com/photo-1551782450-a2132b4ba21d',
      alt: 'Burger',
      title: 'Burger',
      description: 'Beschreibung für Bild 2.',
      author: '@rollelflex_graphy726',
    },
    {
      src: 'https://images.unsplash.com/photo-1444418776041-9c7e33cc5a9c',
      alt: 'Coffee',
      title: 'Coffee',
      description: 'Beschreibung für Bild 3.',
      author: '@nolanissac',
    },
    {
        src: 'https://images.unsplash.com/photo-1558642452-9d2a7deb7f62',
        alt: 'Honey',
        title: 'Honey',
        description: 'Beschreibung für Bild 4.',
        author: '@arwinneil',
      },
    // Füge hier weitere Daten für zusätzliche Bilder hinzu
];

function ImageSlider() {
  return (
    <Carousel interval={3000} pause={false}>
      {sliderData.map((item, index) => (
        <Carousel.Item key={index}>
          <Carousel.Caption className="carousel-caption">
            <h3>{item.title}</h3>
            <p>{item.description}</p>
          </Carousel.Caption>
          <div className="image-container">
            <img className="d-block w-100" src={item.src} alt={item.alt} />
          </div>
        </Carousel.Item>
      ))}
    </Carousel>
  );
}

export default ImageSlider;
